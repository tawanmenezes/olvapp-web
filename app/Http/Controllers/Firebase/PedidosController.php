<?php 
namespace App\Http\Controllers\Firebase;

use Illuminate\Http\Request;
use App\Http\Controllers\Firebase\FirebaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;

use Response, Auth, Session, DB, Redirect;


class PedidosController extends FirebaseController{

    private $orcamento_selecionando;

    public function __construct(){
        parent::__construct();
    }

    /**
     * Obtém todos pedidos
     * @author Tawan
     */
    public function index(){
        $pedidos = $this->database_firebase->getReference('pedidos')->getValue();

        $allPedidos = [];
        foreach($pedidos as $pedido){
            $allPedidos[] = $pedido;
        }

        return view('pages.pedidos.index', [
            'pedidos' =>  $allPedidos,
        ]);
    }

    /**
     * Obtém um registro pelo ID::
     * @author Tawan
     */
    public function show($orcamento_id){
        $orcamentos = $this->database_firebase->getReference('pedidos')->getValue();
        
        $orcamento = [];
        foreach($orcamentos as $key => $value){
            if($key === $orcamento_id){
                $orcamento = $value;
                break;
            }
        }

        return view('pages.pedidos.show', [
            'orcamento' =>  $orcamento,
        ]);
    }

    /**
     * Salvar um Pedido:
     * @author Tawan
     */
    public function store(Request $request){
       
    }

    /**
     * Remover um Pedido:
     * @author Tawan
     */
    public function destroy(Request $request){
     
    }

    /**
     * Alterar uma status do Pedido:
     * @author Tawan
     */
    public function alterarFlag(Request $request){


        $orcamentos = $this->database_firebase->getReference('pedidos')->getValue();
        
        $orcamento = [];
        foreach($orcamentos as $key => $value){
            if($key === $request->orcamento_id){
                $orcamento = $value;
                break;
            }
        }
        $orcamento['status'] = 'Pedido';
        
        
        $orcamento = $this->database_firebase->getReference('pedidos')->update([ $request->orcamento_id => $orcamento ]);
        
        return redirect('home/pedidos/');
    }

}