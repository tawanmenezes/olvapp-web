<?php

namespace App\Http\Controllers\Firebase;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $database_firebase;

    
    public function __construct(){
        $firebaseConn = (new Factory)->withServiceAccount(ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json'))->create();
        $this->database_firebase = $firebaseConn->getDatabase();
    }

   
}