<?php 
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed de usuários
     * @author Edno
     */
    public function run()
    {   
        /**
         * Não dê seed em produção nessa bagaça
         */
        
        Eloquent::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        # ------------------------------------ User admin ------------------------------------
        // Admin
        $user = [
            'name'     => 'Administrador',
            'email'    => 'admin@olv.com',
            'password' => bcrypt('123456'),
            'status' => 1,
        ];
        $user = User::create($user);
    }
}