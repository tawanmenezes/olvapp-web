<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('register', 'Auth\LoginController@showLoginForm'); 


Route::group(['middleware' => 'auth:web'], function(){

   
    Route::group(['prefix' => 'home'], function(){
        Route::get('/', 'HomeController@index')->name('home');

        Route::namespace('Firebase')->group(function () {
            // Pedidos
            Route::group(['prefix' => 'pedidos'], function(){
                Route::get('/',  		     'PedidosController@index');
                Route::post('/alterar-flag', 'PedidosController@alterarFlag');
                Route::get('/{id}',       	 'PedidosController@show');
            });
        });
    });
});
