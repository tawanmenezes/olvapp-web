@extends('adminlte::page')

@section('title', 'OLV APP - Pedidos')

@section('content_header')
    <h1>Todos os pedidos cadastrados.</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ID</th>
              <th>Nome cliente</th>
              <th>Vendedor e-mail</th>
              <th>Valor total</th>
              <th>Valor do desconto</th>
              <th>Metodo de pagamento</th>
              <th>Quantidade parcelas</th>
              <th>Status</th>
              <th>Ação</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($pedidos as $pedido)
                    <tr>
                        <td>{{ $pedido['id'] }}</td>
                        <td>{{ isset($pedido['nomeCliente']) ? $pedido['nomeCliente'] : '' }}</td>
                        <td>{{ $pedido['usuario'] }}</td>
                        <td>{{ $pedido['totalValorProdutos'] }}</td>
                        <td>{{ $pedido['totalDesconto'] }}</td>
                        <td>{{ isset($pedido['tipoPagamento']) ? $pedido['tipoPagamento'] : '' }}</td>
                        <td>{{ isset($pedido['qtdParcelas']) ? $pedido['qtdParcelas'] : '' }}</td>
                        <td>
                            {{--  {{ isset($pedido['status']) ? $pedido['status'] : '' }}  --}}
                            @if($pedido['status'] == 'Pedido')
                                <span class="label label-success">{{ $pedido['status'] }}</span>
                            @elseif($pedido['status'] == 'Orçamento')
                                <span class="label label-warning">{{ $pedido['status'] }}</span>
                            @elseif($pedido['status'] == 'Editando')
                                <span class="label label-primary">{{ $pedido['status'] }}</span>
                            @endif
                        </td>
                        <th> 
                            <a href="{{ url('home/pedidos/'.$pedido['id']) }}" class="btn btn-info">Visualizar Pedido</a>
                        </th>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th>ID</th>
              <th>Nome Cliente</th>
              <th>Vendedor E-mail</th>
              <th>Valor total</th>
              <th>Valor do desconto</th>
              <th>Metodo de Pagamento</th>
              <th>Quantidade parcelas</th>
              <th>Status</th>
              <th>Ação</th>
            </tr>
            </tfoot>
          </table>
        </div>
      </div>
@stop