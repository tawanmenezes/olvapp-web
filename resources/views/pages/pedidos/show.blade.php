@extends('adminlte::page')

@section('title', 'OLV APP - Detalhes do pedido')

@section('content_header')
    <h1>Orçamento numero: {{ $orcamento['id'] }} </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">

            <div class="form-group">
                <label for="nomeCliente">Nome Cliente</label>
                <input type="text" class="form-control" id="nomeCliente" value="{{ $orcamento['nomeCliente'] }}" disabled>
            </div>

            <div class="form-group">
                <label for="tipoPagamento">Tipo Pagamento</label>
                <input type="text" class="form-control" id="tipoPagamento" value="{{ $orcamento['tipoPagamento'] }}" disabled>
            </div>

            <div class="form-group">
                <label for="qtdParcelas">Quantidade de parcelas</label>
                <input type="text" class="form-control" id="qtdParcelas" value="{{ $orcamento['qtdParcelas'] }}" disabled>
            </div>

            <br><br>
            
            <label for="produtos">Produtos</label>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Marca</th>
                        <th>Quantidade</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orcamento['produtos'] as $produto)
                        <tr>
                            <td>{{ $produto['id'] }}</td>
                            <td>{{ $produto['nome']}}</td>
                            <td>{{ $produto['marca'] }}</td>
                            <td>{{ $produto['qtdProdutoPedido'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
            <br><br>
        
            <div class="form-group">
                <label for="totalDesconto">Valor do desconto</label>
                <input type="text" class="form-control" id="totalDesconto" value="{{ $orcamento['totalDesconto'] }}" disabled>
            </div>
            
            <div class="form-group">
                <label for="totalValorProdutos">Subtotal</label>
                <input type="text" class="form-control" id="totalValorProdutos" value="{{ $orcamento['totalValorProdutos'] }}" disabled>
            </div>


            <div class="form-group">
                <label for="totalValorProdutosComDesconto">VALOR TOTAL</label>
                <input type="text" class="form-control" id="totalValorProdutosComDesconto" value="{{ $orcamento['totalValorProdutosComDesconto'] }}" disabled>
            </div>

            @if($orcamento['status'] != 'Pedido' )
                <form action="{{ url('/home/pedidos/alterar-flag') }}" method="POST">
                    @csrf
                    <input type="hidden" name="orcamento_id" value="{{ $orcamento['id'] }}">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Confirma orçamento</button>
                </form>
            @endif


        </div>
      </div>
@stop