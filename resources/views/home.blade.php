@extends('adminlte::page')

@section('title', 'OLV APP')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p><b>Seja Bem vindo</b> {{ Auth::user()->name }} !!</p>
@stop